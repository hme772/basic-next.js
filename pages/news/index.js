import Link from "next/link";
import { Fragment } from "react";
const NewsPage = () => {
    return <Fragment>
        <h1>The News page</h1>
        <ul>
            <li><Link href="/news/next">NextJS is a great framework</Link></li>
            <li><Link href="/news/kuku">kuku</Link></li>
        </ul>
    </Fragment>
};

export default NewsPage;